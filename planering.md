# Planering

* Intro 
	* Översikt
	* Historia
	* Svag & stark AI
	* AI jämförelser chatbottar LLMS 
* Teori & praktik
	* Översikt AI områden 
		* AI områden (ML, NN, DL etc)
		* Data (bias, säkerhet etc)
		* Python 2-3 veckor
	* Neurala nätverk
		* Object Detection
			* Teori NN Object Detection
			* Praktik Object Detection
				* LAB Teachable Machine [https://teachablemachine.withgoogle.com/train]
				* LAB Python Object Detection 
		* LLMs
			* Teori LLMs
			* Agenter [https://learn.microsoft.com/en-us/azure/cosmos-db/ai-agents]
			* Användning LLMs tekniker
				* Prompt Engineering
				* RAG
				* Fine Tuning
			* Praktik LLMs
				* Chat GPT prompt engineering för att lösa komplext problem
			* Praktik lokala modeller
				* GPT4All /LMStudio
					* Ladda ned lokal modell
					* Uppgift Skapa support bot för ngt ändamål med LLM tekniker
				* Python labb 
					* Skapa program som uttnytjar samma lokala modell 
		* Machine Learning
			* Teori ML
				* Övervakad inlärning / oövervakad inlärning
				* Stegen i att träna modell
			* WEKA WORKBENCH LAB
			* Python LAB
	* AI & samhälle 
		* Etik: Three Laws of Robotics etc
		* Uppgift Desinformationskamapanj 
		


