# Lagar och regler

## Syfte

Syftet med detta avsnitt är att ge en översikt av de lagar och regler som reglerar användningen av AI i samhället.

## Innehåll

- [GDPR](#gdpr)
- [Personuppgiftslagen](#personuppgiftslagen)
- [Dataskyddsförordningen](#dataskyddsförordningen)
