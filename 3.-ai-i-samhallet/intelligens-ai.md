# Jämförelse av mänsklig och artificiell intelligens

## Mål
* Utforska skillnaderna och likheterna mellan mänsklig och artificiell intelligens.
* Diskutera hur AI kan komplettera och förbättra mänskliga förmågor.

## Föreläsning

### Mänsklig intelligens
* Mänsklig intelligens är en komplex förmåga som omfattar kognition, perception, inlärning, problemlösning och kreativitet.
* Människor har förmågan att anpassa sig till nya situationer, lära sig av erfarenhet och skapa nya idéer och lösningar.

### Artificiell intelligens
* Artificiell intelligens är datorbaserad intelligens som efterliknar mänskliga kognitiva funktioner.
* AI-system kan utföra specifika uppgifter snabbare och mer effektivt än människan

{% embed url="https://youtu.be/Pv0cfsastFs?si=u8c33heIv_uMQM7O" %}

## Aktiviteter

### Jämförelse av mänsklig och artificiell intelligens
* Be eleverna skapa en lista över skillnader och likheter mellan mänsklig och artificiell intelligens.
* Diskutera hur AI kan användas för att komplettera och förbättra mänskliga förmågor, som i medicin, forskning och kreativt skapande.