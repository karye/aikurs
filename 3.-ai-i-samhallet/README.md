# 3. AI i Samhället

* Jämförelse mellan hur enklare lösningar med AI fungerar och hur en människa löser samma problem.
* Några situationer där AI är överlägsen människan och tvärtom.
* Normer samt lagar och andra bestämmelser som gäller AI och dess användning.
* Etiska dilemman med användandet av AI, däribland transparens.
* Demokratiska, sociala, ekonomiska, miljömässiga och säkerhetsmässiga möjligheter och risker med AI-användning samt dess konsekvenser för samhället.
