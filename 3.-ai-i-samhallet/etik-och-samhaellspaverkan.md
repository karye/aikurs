# Samhällspåverkan och etiska frågor

## Mål
Denna lektion syftar till att ge eleverna en förståelse för de komplexa etiska och sociala frågorna som omger AI. Genom att utforska realistiska scenarier, uppmuntras eleverna att tänka kritiskt om AI:s roll i samhället och hur de som framtida utvecklare eller användare av tekniken kan bidra till en mer ansvarsfull användning.

**Innehåll för Föreläsningen**:

{% embed url="https://youtu.be/q6U9mhKAFFA?si=ywBadUjlKmNUJPYZ" %}

* **Etiska utmaningar inom AI**: Introducera olika etiska frågor relaterade till AI, såsom bias i algoritmer, integritet, och automatiseringens påverkan på arbetsmarknaden.
* **Samhällspåverkan**: Diskutera hur AI påverkar samhället i stort, inklusive både positiva effekter (till exempel effektivitetsförbättringar, hjälp inom hälsovården) och negativa effekter (arbetslöshet, övervakning).
* **Ansvarsfull AI**: Ta upp vikten av att utveckla och använda AI på ett ansvarsfullt sätt, och introducera koncept som transparent AI och rättvisa algoritmer.

## Aktivitet

* **Organisera en desinformationskampanj mot ett land:**
  * Dela in i grupper om tre elever.
  * Varje grupp arbetar på ett påverkanskampanj mot ett specifikt land.
  * Grupperna presenterar sina arbeten i 5-10 min.
* **Etikdiskussion i grupper**:
  * Dela in klassen i grupper och ge varje grupp ett etiskt dilemma relaterat till AI (t.ex., användning av AI i övervakning, AI i krigföring, AI och arbetslöshet).
  * Låt grupperna diskutera och förbereda argument för och emot, och sedan hålla en klassdebatt.
  * Resurs: [AI Ethics](https://www.weforum.org/agenda/2020/10/how-to-ensure-ethical-ai/)
* **Presentation och gruppdiskussion**: Varje grupp presenterar sina tankar och lösningar för klassen, följt av en öppen diskussion där alla deltar.
  * Hur kan vi minimera bias i AI-algoritmer?
  * Vilket ansvar har utvecklare och företag för att säkerställa att AI används på ett etiskt sätt?
* **Sammanfattning och reflektion**: Sammanfatta huvudpunkterna från gruppernas presentationer och diskutera hur dessa insikter kan tillämpas i praktiken.
* **Lärarens feedback**: Kommentera och komplettera elevernas analyser och förslag med ytterligare perspektiv och exempel.
