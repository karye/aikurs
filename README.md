# Centralt innehåll

### Artificiell intelligens

* Definition av AI samt centrala begrepp inom området.
* Vikten av data, datakvalitet för AI och val av data.
* Drivkrafter bakom utvecklingen av AI.
* Översikt över användningen av AI, däribland prediktion, robotik, vision och generativ AI samt olika tekniker och metoder som ligger bakom dessa.
* Översikt av tekniker för AI, däribland sökning, klassificering och objekt­igenkänning.
* Översikt av metoder och algoritmer inom AI, däribland beslutsträd, re­gression samt övervakat och oövervakat lärande.

### Praktisk användning av AI

* Översikt över processen för problemlösning med AI.
* Enklare typ av problemlösning med hjälp av AI, till exempel:
  * klassificering
  * objektigenkänning
  * prediktion
  * tolkning och bearbetning av naturligt språk (NLP)
  * enklare maskininlärning och användning av spelagent.
* Principer för, och tillämpningar av, vanliga tekniker inom AI, däribland maskininlärning och robotik.
* Metoder för enklare träning av AI.

### Förhållningssätt till AI

* Jämförelse mellan hur enklare lösningar med AI fungerar och hur en människa löser samma problem.
* Några situationer där AI är överlägsen människan och tvärtom.
* Normer samt lagar och andra bestämmelser som gäller AI och dess användning.
* Etiska dilemman med användandet av AI, däribland transparens.
* Demokratiska, sociala, ekonomiska, miljömässiga och säkerhetsmässiga möjligheter och risker med AI-användning samt dess konsekvenser för samhället.
* Jämförelse av mänsklig och artificiell intelligens

