# Grundläggande AI-koncept

## Mål
* Introducera grundläggande AI-koncept som maskininlärning, neurala nätverk och algoritmer.
* Utforska hur dessa koncept används för att skapa AI-system som kan utföra uppgifter som normalt kräver mänsklig intelligens.

![Svag vs stark AI](../.gitbook/assets/image-4.png)

## Föreläsning

{% embed url="https://youtu.be/Pv0cfsastFs?si=rladqvmp0CO3cK5V&t=815" %}

### Grundläggande AI-koncept

Artificiell intelligens (AI) är ett brett fält som omfattar många olika tekniker och algoritmer som används för att skapa system som kan utföra uppgifter som normalt kräver mänsklig intelligens. Här är några grundläggande AI-koncept som är viktiga att förstå:

#### 1. Artificiell intelligens (AI)
   - **Definition**: AI är det övergripande fältet som omfattar all teknik och forskning som syftar till att skapa system som kan utföra uppgifter som normalt kräver mänsklig intelligens.
   - **Exempel**: Självkörande bilar, röstassistenter, bildigenkänning.

#### 2. Svag/smal AI
   - **Definition**: Svag AI är en form av AI som är specialiserad på att utföra en specifik uppgift eller en uppsättning av uppgifter. Den har ingen generell intelligens eller medvetenhet.
   - **Exempel**: Ansiktsigenkänning, rekommendationssystem på Netflix.

#### 3. Stark/bred AI
   - **Definition**: Stark AI, även känd som generell eller bred AI, är en hypotetisk form av AI som skulle kunna utföra alla intellektuella uppgifter som en människa kan, inklusive förståelse och resonemang.
   - **Exempel**: Denna form av AI har ännu inte utvecklats och är fortfarande en teoretisk idé.

### Grundläggande AI-tekniker

Det finns flera tekniker och algoritmer som används inom AI för att skapa intelligenta system. Här är några av de vanligaste:

#### 1. Maskininlärning (Machine Learning)
   - **Definition**: Maskininlärning är en metod inom AI som fokuserar på att skapa system som kan lära sig från data utan att vara explicit programmerade för varje specifik uppgift.
   - **Exempel**: Spamfilter i e-post, automatiserade system för aktiehandel.

#### 2. Neurala nätverk (Neural Networks)
   - **Definition**: Neurala nätverk är en specifik teknik inom maskininlärning som är inspirerad av den mänskliga hjärnans struktur. De används för att modellera komplexa mönster och relationer i data.
   - **Exempel**: Bildklassificering, taligenkänning.

#### 3. LLM (Large Language Models)
   - **Definition**: LLM (Large Language Models) är en typ av AI-modell som är tränad på stora mängder textdata för att generera naturligt språkstext. De används för uppgifter som textgenerering, översättning och sammanfattning.
   - **Exempel**: GPT-3, BERT.

### Organiserad översikt

![AI trädet](../.gitbook/assets/image-3.png)

## Aktivitet

* **Kahoot Quiz**:
  * Skapa ett quiz på [Kahoot](https://kahoot.com/) med frågor om de grundläggande AI-koncept som diskuterades under föreläsningen. Detta engagerar eleverna och hjälper till att förstärka lärandet.
  * Exempel på frågor:
    * Vad är skillnaden mellan **svag/smal** AI och **stark/bred** AI?
    * Vad är **maskininlärning** och hur skiljer det sig från traditionell programmering?
    * Vad är **neurala nätverk** och hur används de inom AI?
    * Vilka är några exempel på **LLM** och hur används de för att generera text?
* **Gruppdiskussioner**:
  * Efter quizet, dela in eleverna i små grupper och be dem diskutera hur de grundläggande AI-koncepten kan tillämpas i olika branscher, som medicin, transport och underhållning.
  * Ge varje grupp en specifik bransch att fokusera på och låt dem presentera sina diskussioner för klassen.
* **Reflektion och Fördjupning**:
  * Diskutera quizresultaten och klargör eventuella missförstånd.
  * Be eleverna ge exempel på hur de tror att AI kan påverka deras framtida karriärer och vardagsliv.
  * Artikel för reflektion: [The Future of AI](https://www.forbes.com/sites/forbestechcouncil/2021/07/29/the-future-of-artificial-intelligence/)

## Vill du veta mer?

### Neurala nätverk
{% embed url="https://www.youtube.com/watch?v=aircAruvnKk" %}

{% embed url="https://youtu.be/J4Qsr93L1qs?si=BPJstbdhZE_KxH3k" %}

{% embed url="https://youtu.be/PeMlggyqz0Y?si=7E0V1BZ837d9BBEH" %}