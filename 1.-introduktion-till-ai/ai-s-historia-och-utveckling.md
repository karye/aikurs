# AI:s historia och utveckling

## Mål
* Ge en översikt av AI:s historia och utveckling.
* Utforska viktiga händelser och personer som har påverkat AI:s utveckling.

## Föreläsning
* Kort översikt av AI:s utveckling från de tidiga början med Alan Turing till moderna neurala nätverk.

{% embed url="https://youtu.be/q6U9mhKAFFA?si=ywBadUjlKmNUJPYZ" %}

### Alternativ 1 kortare video om AI:s historia

{% embed url="https://www.youtube.com/watch?v=NGZx5GAUPys" %}

### Alternativ 2 kortare video om AI:s historia

{% embed url="https://www.youtube.com/watch?v=yaL5ZMvRRqE" %}

## Aktivitet

### Tidslinjeprojekt
  * Skapa en visuell tidslinje som illustrerar viktiga händelser i AI:s historia. 
  * Skapa en tidslinje med [TimelineJS och Google kalkylblad](https://timeline.knightlab.com/#make)

### Diskussionsfrågor
  * Vilken händelse eller person tycker du är mest betydelsefull i AI:s historia och varför?
  * Hur tror du att tidigare uppfattningar och hinder inom AI har påverkat dess utveckling?
  * Artikel för reflektion: [Milestones in the History of AI](https://medium.com/higher-neurons/10-historical-milestones-in-the-development-of-ai-systems-b99f21a606a9)

### Reflektion och presentation
  * Presentera din tidslinje och dela dina tankar kring de diskussionsfrågor som givits. 

## Vill du veta mer?

{% embed url="https://youtu.be/R3YFxF0n8n8?si=EK8nZQLMWMldSxOP" %}