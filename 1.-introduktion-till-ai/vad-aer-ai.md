# Vad är AI?

## Mål
* Introducera AI och dess användningsområden.
* Utforska hur AI kan användas för att skapa musik, konst och andra kreativa verk.

## Föreläsning
* AI kan användas för att skapa musik, konst och andra kreativa verk.
* Generativ AI är en ny teknik som gör det möjligt för datorer att skapa kreativa verk.
* AI kan vara ett hot eller en möjlighet för den mänskliga kreativiteten.

{% embed url="https://www.svtplay.se/video/KNw75xd/generation-ai/1-kreativa-maskiner?video=visa" %}

## Aktiviteter - prova på AI

Parvis eller i grupp, prova på olika AI-applikationer och diskutera resultaten.

1. **Quick, Draw!**
    * Testa [Quick, Draw!](https://quickdraw.withgoogle.com/) där du får rita en bild som AI sedan försöker gissa. Detta visar AI:s förmåga att lära sig igenkänna olika objekt.
    * Instruktioner: Rita olika objekt och se hur AI tolkar dem. Diskutera resultaten.
2. **AI Duet**
    * Använd [AI Duet](https://experiments.withgoogle.com/ai/ai-duet/view/) för att utforska hur AI kan interagera med musik. Du spelar en melodi och AI svarar, vilket demonstrerar AI:s förmåga att interagera och svara på mänskliga inslag.
    * Instruktioner: Spela enkla melodier och analysera hur AI svarar.
3. **Shadow Art**
    * Prova [Shadow Art](https://experiments.withgoogle.com/shadow-art), där du använder dina händer för att skapa skuggfigurer som AI sedan identifierar. Detta är ett roligt sätt att utforska AI och datorseende.
    * Instruktioner: Skapa olika skuggfigurer och se hur bra AI identifierar dem.
4. **Skapa bilder från text**
    * Använd [Copilot](https://www.bing.com/images/create) för att skapa bilder från textbeskrivningar.
    * Instruktioner: Skriv olika textbeskrivningar och se hur AI tolkar dem.
5. **Skapa ditt egna alfabet med Google gentype**
    * Använd [Google gentype](https://labs.google/gentype) för att skapa ditt egna alfabet.
    * Instruktioner: Skriv in en prompt och se hur AI genererar text baserat på den.
6. **Skapa musik från text**
    * Använd [ampermusic](https://www.ampermusic.com/) för att skapa musik från textbeskrivningar. 
    * Instruktioner: Skriv olika textbeskrivningar och lyssna på den genererade musiken.

### Diskussionsfrågor

* Efter experimenten, diskutera hur dessa AI-applikationer liknar eller skiljer sig från varandra.
* Vilka AI-teknologier använder du dagligen och hur fungerar de?
* Hur tror ni att AI kommer att förändras och påverka våra liv i framtiden?