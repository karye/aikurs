# 1. AI och historia

## Vad är AI?

Artificiell intelligens (AI) är ett forskningsområde som syftar till att skapa maskiner som kan tänka och agera som människor. AI är en del av datavetenskapen och omfattar en mängd olika tekniker och metoder för att skapa intelligenta system. AI kan användas för att lösa en mängd olika problem, som att förutsäga framtida händelser, tolka och bearbeta naturligt språk, och att agera i en miljö.

## Ett kort historiskt perspektiv

Artificiell intelligens (AI) är ett forskningsområde som har sina rötter i 1950-talet. Redan då fanns det visionärer som såg möjligheterna med att skapa maskiner som kunde tänka och agera som människor. En av de första som formulerade idén om artificiell intelligens var matematikern Alan Turing, som 1950 publicerade en artikel där han diskuterade möjligheten att skapa maskiner som kunde tänka. Turing för priset av att vara en av de första att föreslå att maskiner skulle kunna tänka och agera som människor.

## Centrala begrepp inom AI

Inom AI finns det en mängd olika begrepp och termer som är centrala för att förstå området. Här är några av de viktigaste:

* **Maskininlärning**: En del av AI som handlar om att skapa system som kan lära sig från data.
* **Djupinlärning**: En gren av maskininlärning som fokuserar på att skapa system som kan lära sig att lösa komplexa problem genom att använda flera lager av neuroner.
* **Neuronnät**: En modell för att skapa maskininlärningssystem som är inspirerad av hur hjärnan fungerar.
* **Övervakat lärande**: En typ av maskininlärning där systemet tränas på data där rätt svar är känt.
* **Oövervakat lärande**: En typ av maskininlärning där systemet tränas på data där rätt svar inte är känt.
* **Träningsdata**: Den data som används för att träna ett maskininlärningssystem.
* **Testdata**: Den data som används för att testa ett maskininlärningssystem.
* **Modell**: En representation av ett maskininlärningssystem som har tränats på data.
* **Klassificering**: En typ av maskininlärning där systemet tränas på data för att kunna klassificera nya exempel i olika kategorier.
* **Regression**: En typ av maskininlärning där systemet tränas på data för att kunna förutsäga en kontinuerlig variabel.
