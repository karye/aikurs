# Kursupplägg för AI-kurs

1. **Introduktion till AI**  
   - **Vad är AI och varför är det viktigt?**  
   Vi börjar med att definiera vad AI egentligen är. Vi tittar på hur datorer kan lösa problem och ta beslut, något som vi kallar för artificiell intelligens (AI). Vi kommer också att prata om hur AI har utvecklats genom historien, från enkla datorer till dagens avancerade AI-system som kan lära sig och förbättras över tid.
  
2. **Vad är AI?**  
   - **Grundläggande tekniker och trender inom AI**  
   Vi kommer att utforska de tekniker som gör AI möjligt. Vi pratar om maskininlärning, där AI tränas på stora mängder data för att fatta beslut, och vi kollar på aktuella trender inom AI, som självkörande bilar och röststyrda assistenter. Varför har AI blivit så stort just nu, och vad betyder det för framtiden?

3. **Utforska AI-bottar**  
   - **Praktiska exempel på AI-system**  
   Vi lär känna olika AI-bottar som används i verkligheten, som ChatGPT och Gemini. Ni kommer att få testa dessa system och se hur de fungerar. Hur kan dessa bottar hjälpa till med att lösa problem? Vi pratar också om vilka utmaningar som finns med att använda AI på detta sätt.

4. **Textbottar och prompt-engineering**  
   - **Skapa effektiva instruktioner för AI**  
   Här lär vi oss hur man kan prata med en AI. Vi lär oss att skriva "prompts", eller instruktioner, för att få AI:n att ge rätt svar. Vi diskuterar också hur AI genererar text, och varför den ibland gör misstag. Hur kan vi göra våra instruktioner så tydliga som möjligt?

5. **Filosofi om AI**  
   - **Vad är AI, egentligen?**  
   Vi diskuterar vad intelligens betyder och om en maskin kan vara intelligent. Vi tar upp **Turing-testet**, där en AI bedöms intelligent om den kan konversera så bra att en människa inte kan avgöra om det är en maskin. Vi pratar också om **Kinesiska rummet**, ett tankeexperiment som ifrågasätter om AI verkligen förstår vad den gör eller bara följer instruktioner utan medvetenhet.

6. **Data och datakvalitet i maskininlärning**  
   - **Varför är data viktigt för AI?**  
   AI blir bara så bra som den data den tränas på. Här lär vi oss att förstå vad som gör data bra eller dålig, och hur viktig datakvalitet är för att AI ska kunna fatta bra beslut. Vi tittar också på hur man väljer rätt data när man tränar AI-modeller.

7. **Bygg och träna en AI-modell**  
   - **Skapa din egen AI**  
   Med hjälp av ett verktyg som heter Teachable Machine får ni bygga en enkel AI-modell. Ni kommer att lära er att skapa data och använda den för att träna AI. Detta ger er en praktisk förståelse för hur AI faktiskt fungerar.

8. **Maskininlärning – Introduktion**  
   - **Vad är maskininlärning?**  
   Vi pratar om hur AI kan "lära sig" saker genom att analysera data. Ni kommer att få en grundläggande förståelse för de olika tekniker och metoder som används inom AI. Det är den här tekniken som gör att AI kan förutsäga saker, känna igen bilder och mycket mer.

9. **Övervakat och oövervakat lärande**  
   - **Två olika sätt för AI att lära sig**  
   Här går vi igenom skillnaderna mellan övervakat och oövervakat lärande. Vid övervakat lärande har AI:n tydliga exempel att träna på, medan vid oövervakat lärande försöker AI själv hitta mönster i datan. Vi kommer också att prata om vilka typer av problem dessa tekniker är bra på att lösa.

10. **Skapa god data för övervakat lärande**  
    - **Manuell datainsamling**  
    I denna uppgift får ni själva skapa och klassificera data genom att till exempel kategorisera filmer. Ni får skapa ett beslutsträd och analysera datans kvalitet. Hur kan ni se till att den data ni använder är bra nog för att AI ska kunna lära sig effektivt?

11. **Oövervakat lärande**  
    - **När AI lär sig utan exempel**  
    Vi tittar närmare på hur AI kan klustra data och identifiera mönster på egen hand utan att få några tydliga exempel. Vi går igenom hur tekniker som klustring och självorganiserande kartor fungerar, och hur dessa kan användas i praktiska situationer.

12. **Neurala nätverk och djupinlärning**  
    - **Hur AI efterliknar den mänskliga hjärnan**  
    Här lär vi oss om neurala nätverk, som är ett sätt för AI att bearbeta information ungefär som den mänskliga hjärnan. Ni får en introduktion till djupinlärning, som är en avancerad form av AI som används i allt från bildigenkänning till självkörande bilar.

13. **Generera bilder med AI**  
    - **AI och kreativitet**  
    I denna praktiska uppgift får ni använda AI för att skapa bilder utifrån textbeskrivningar. Hur kan AI användas inom konst och design? Vi diskuterar också vad detta betyder för framtidens kreativa yrken.

14. **Utforska AI-tillämpningar**  
    - **AI i olika sektorer**  
    I grupper kommer ni att diskutera hur AI används inom olika områden som sjukvård, industri, och underhållning. Vi diskuterar också hur dessa AI-tekniker påverkar samhället idag och vilka framtidsscenarier som kan bli aktuella.

15. **NLP (Naturlig språkbehandling) med AI**  
    - **AI som förstår och skapar text**  
    Vi går igenom grunderna för NLP, som gör att AI kan förstå och generera naturligt språk, som när ni pratar med Siri eller Google Assistant. Vad är utmaningarna med att få en dator att förstå mänskligt språk?

16. **Skapa ett 2D-spel med AI**  
    - **Bygg ett spel med AI**  
    Ni får skapa ett enkelt 2D-spel i Python där ni implementerar AI för att kontrollera olika spelelement. Fokus ligger på att förstå hur AI kan användas i spel och hur ni kan lösa problem genom att använda AI.

17. **Skapa en låt med AI**  
    - **AI och musikgenerering**  
    Här använder vi AI-verktyg för att skapa en egen låt. Vi diskuterar också hur AI påverkar kreativa områden som musikindustrin, och vilka möjligheter och utmaningar som finns med AI-genererad musik.

18. **AI och spelagenter**  
    - **Träna en spelagent**  
    Vi går igenom hur AI används i spelvärlden, från klassiska exempel som schack och Go, till moderna spelagenter. Ni kommer att få träna en enkel spelagent och diskutera hur AI kan användas för att skapa smartare motståndare i spel.

19. **Lagar och bestämmelser inom AI**  
    - **Regler för AI**  
    Vi tittar på de lagar och regler som styr AI, till exempel GDPR och andra sektorsspecifika lagar. Vad får man och vad får man inte göra med AI? Vi diskuterar också hur lagarna påverkar utvecklingen av AI.

20. **Etik och transparens inom AI**  
    - **AI och rättvisa**  
    Vi utforskar etiska frågor som rör AI, som rättvisa, bias, integritet och transparens. Hur kan vi se till att AI-system är rättvisa och inte diskriminerar? Vad betyder det att AI är transparent, och varför är det viktigt?

21. **Samhället med AI**  
    - **AI:s påverkan på samhället**  
    Vi analyserar hur AI påverkar samhället, både vad gäller demokrati, ekonomi, säkerhet och miljö. Vilka risker finns det, och vilka möjligheter öppnar AI upp för?

22. **Filmer om AI**  
    - **AI i populärkulturen**  
    Vi kommer att titta på filmer som "Blade Runner" och "Ex Machina" och diskutera hur dessa filmer behandlar AI. Vad säger dessa filmer om AI:s möjligheter och risker, och vilka etiska frågor tar de upp?
