# Idéer

* Labb med maskininlärning (python, data, regression)
* Hur bra är [https://deepseekcoder.github.io](https://deepseekcoder.github.io/) ?
* Elever installerar på sina datorer [https://github.com/zylon-ai/private-gpt](https://github.com/zylon-ai/private-gpt)
* Elever installerar en modell lokalt
  * Anpassar den: [https://github.com/manfred-lindmark/gpt-sw3-finetune](https://github.com/manfred-lindmark/gpt-sw3-finetune)
  * Anropar den via python-api
  
* Kolla upp [https://github.com/lm-sys/FastChat?tab=readme-ov-file#fine-tuning](https://github.com/lm-sys/FastChat?tab=readme-ov-file#fine-tuning)

* [Image classification from scratch](https://keras.io/examples/vision/image_classification_from_scratch/)
