# Introduktion till maskininlärning

## Mål
Denna lektion är tänkt att ge en praktisk och interaktiv introduktion till maskininlärning. Genom att själva skapa och träna en enkel AI-modell får eleverna en hands-on förståelse för koncepten och utmaningarna inom maskininlärning.

## Intro till maskininlärning
[**https://machinelearningforkids.co.uk/#!/welcome**](https://machinelearningforkids.co.uk/#!/welcome)

{% file src="../../.gitbook/assets/lek 2 intro maskininlärning.pptx.pdf" %}

## Innehåll

* **Vad är maskininlärning?**: Förklara grundläggande principer för maskininlärning och hur det skiljer sig från traditionell programmering.
* **Typer av maskininlärning**: Introducera begreppen övervakat lärande, oövervakat lärande och förstärkningslärande med enkla exempel.
* **Tillämpningar av maskininlärning**: Visa exempel på hur maskininlärning används i vardagen, som i produktrekommendationer, bildigenkänning och prediktiv analys.
* **Grundläggande maskininlärningsalgoritmer**: Ge en översikt av några enkla algoritmer som beslutsträd och linjär regression.

## Aktivitet

* **Bygg en enkel maskininlärningsmodell**: Använda en webbaserad plattform som [Google's Teachable Machine](https://teachablemachine.withgoogle.com/) för att låta eleverna skapa och träna en enkel modell. De kan till exempel skapa en modell som kan känna igen och klassificera olika handgester eller enkla objekt.
* **Reflektion och diskussion**: Efter aktiviteten, diskutera erfarenheterna av att träna modellen.
  * Vad var lätt eller svårt?
  * Hur påverkade datan som användes resultatet?
  * Hur kan den modell ni skapade användas i ett verkligt scenario?
  * Vilka faktorer tror ni påverkar en maskininlärningsmodells noggrannhet och effektivitet?
* **Lärarens feedback**: Ge feedback på elevernas arbete och diskutera gemensamma utmaningar och lösningar som uppstått under aktiviteten.
