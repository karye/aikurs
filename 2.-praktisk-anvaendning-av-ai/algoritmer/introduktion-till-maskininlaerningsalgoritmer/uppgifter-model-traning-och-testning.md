# Uppgifter -  Model träning och testning

**Steg för modell-träning och testning**

1. **Välja modell/algoritm**
2. **Träna modeller**
3. **Testa modeller**
4. **Spara bästa modellen**
5. **Ladda upp bästa modellen**
6. **Använd bästa modellen**

För steg 2-4 måste en loop användas som skapar och tränar modeller där den bästa sparas.

## Filer

{% file src="../../../.gitbook/assets/USA_Housing_ny_data.csv" %}

{% file src="../../../.gitbook/assets/London_ny_data (1).csv" %}

### UPPGIFT A: Fortsättning USA housing

Du ska fortsätta med din gamla Python-fil från tidigare uppgift.

**Steg för modell-träning och testning**

1.  **Välja modell/algoritm och läs in den**

    * Vi vill att modellen ska förutsäga priser på hus i USA, så vi använder en linjär regressionsmodell.

    ```python
    from sklearn.linear_model import LinearRegression
    ```
2.  **Skapa modellen**

    ```python
    # Skapa modellen
    model = LinearRegression()
    ```
3.  **Träna modellen**

    ```python
    # Träna modellen
    model.fit(X_train, Y_train)
    ```
4.  **Testa modellen och spara resultatet**

    ```python
    # Testa modellen
    score = model.score(X_test, Y_test)
    print(f'Modellens noggrannhet: {score}')
    ```
5.  **Skapa LOOP**

    * För att träna och testa flera modeller och spara den bästa, skapa en loop som delar datat, tränar och testar modellen. Den bästa modellen sparas på fil.

    ```python
    import joblib
    best_score = 0
    best_model = None

    for _ in range(10):
        X_train, X_test, Y_train, Y_test = train_test_split(X, Y, test_size=0.2, random_state=42)
        model = LinearRegression()
        model.fit(X_train, Y_train)
        score = model.score(X_test, Y_test)
        
        if score > best_score:
            best_score = score
            best_model = model

    joblib.dump(best_model, 'best_model.pkl')
    ```
6.  **Ladda upp bästa modellen från fil**

    ```python
    best_model = joblib.load('best_model.pkl')
    ```
7.  **Använd bästa modellen på nytt data**

    * Hämta det nya datat från classroom, ta bort onödiga kolumner (adresser), och använd bästa modellen för att göra förutsägelser.

    ```python
    new_data = pd.read_csv('new_USA_Housing.csv')
    new_data = new_data.drop(['Address'], axis=1)
    predictions = best_model.predict(new_data)
    print(predictions)
    ```
8.  **Skriv ut funktionens intercept och koefficienter**

    ```python
    print(f'Intercept: {best_model.intercept_}')
    print(f'Koefficienter: {best_model.coef_}')
    ```

### UPPGIFT B: Fortsättning London housing

Du ska fortsätta med att arbeta med data och modeller för att värdera bostadsobjekt i London.

**Steg för modell-träning och testning**

1.  **Välja modell/algoritm och läs in den**

    ```python
    from sklearn.linear_model import LinearRegression
    ```
2.  **Skapa modellen**

    ```python
    model = LinearRegression()
    ```
3.  **Skapa LOOP**

    * Skapa en loop som delar data i träningsdata och testdata, tränar och testar modeller, och sparar den bästa modellen.

    ```python
    import joblib
    best_score = 0
    best_model = None

    for _ in range(10):
        X_train, X_test, Y_train, Y_test = train_test_split(X_london, Y_london, test_size=0.2, random_state=42)
        model = LinearRegression()
        model.fit(X_train, Y_train)
        score = model.score(X_test, Y_test)
        
        if score > best_score:
            best_score = score
            best_model = model

    joblib.dump(best_model, 'best_model_london.pkl')
    ```
4.  **Ladda upp bästa modellen från fil**

    ```python
    best_model_london = joblib.load('best_model_london.pkl')
    ```
5.  **Ladda ned nytt data utan priser från classroom och ta bort onödiga kolumner**

    ```python
    new_data_london = pd.read_csv('new_London.csv')
    new_data_london = new_data_london.drop(['Property Name'], axis=1)
    ```
6.  **Använd bästa modellen på ny data**

    ```python
    predictions_london = best_model_london.predict(new_data_london)
    print(predictions_london)
    ```
7.  **Skriv ut funktionens intercept och koefficienter**

    ```python
    print(f'Intercept: {best_model_london.intercept_}')
    print(f'Koefficienter: {best_model_london.coef_}')
    ```

### Sammanfattning

* Du har nu tränat och testat modeller för att förutsäga huspriser både i USA och London.
* Du har använt linjär regression för att skapa modeller, testat dem, och sparat den bästa modellen.
* Du har laddat upp och använt den bästa modellen på nytt data för att göra förutsägelser.
* Du har skrivit ut funktionens intercept och koefficienter för att förstå modellens påverkan.
