# Övning - KNN: Se skillnad på blom-typer

I den här övningsuppgiften ska du träna en modell att se skillnad på typer av blomman iris. Iris-blomman har tre olika typer: ”Iris-setosa”, ”Iris-versicolor” och ”Iris-virginica”. Genom att mäta längden och bredden på petal och sepal-bladen kan blommorna klassificeras.

{% file src="../../../.gitbook/assets/iris.data" %}

{% file src="../../../.gitbook/assets/iris_new.data" %}

## Steg för att genomföra övningen

1. **Starta PyCharm och skapa ett projekt som använder din maskininlärningsmiljö.**
2. **Skapa en Python-fil.**
3. **Ladda ned filen `iris.data` och lägg in den i ditt projekt.**

### Databeskrivning

Datafilen `iris.data` innehåller följande kolumner:

* `sepal_length`: sepal-bladens längd i cm
* `sepal_width`: sepal-bladens bredd i cm
* `petal_length`: petal-bladens längd i cm
* `petal_width`: petal-bladens bredd i cm
* `class`: Vilken blom-typ blomman tillhör: `Iris-setosa`, `Iris-versicolor` eller `Iris-virginica`

### Ladda in data och förbered data

1.  Läs in data till programmet

    ```python
    import pandas as pd

    # Läs in data från fil
    df = pd.read_csv('iris.data', header=None, names=['sepal_length', 'sepal_width', 'petal_length', 'petal_width', 'class'])
    print(df.head())
    ```
2.  Gör om `class` kolumnens text-data till tal

    ```python
    from sklearn.preprocessing import LabelEncoder

    le = LabelEncoder()
    df['class'] = le.fit_transform(df['class'])
    print(df.head())
    ```
3.  Dela upp data i Label och Features

    ```python
    X = df.drop('class', axis=1)
    Y = df['class']
    ```
4.  Dela upp data i 90% träningsdata och 10% testdata

    ```python
    from sklearn.model_selection import train_test_split

    X_train, X_test, Y_train, Y_test = train_test_split(X, Y, test_size=0.1, random_state=42)
    ```

### Skapa och träna KNN-modellen

1.  Skapa en KNN-modell med K=9 (n\_neighbors=9)

    ```python
    from sklearn.neighbors import KNeighborsClassifier

    knn = KNeighborsClassifier(n_neighbors=9)
    ```
2.  Träna modellen

    ```python
    knn.fit(X_train, Y_train)
    ```

### Testa modellen och skriv ut testresultatet

1.  Testa modellen med testdata och skriv ut träffsäkerheten

    ```python
    accuracy = knn.score(X_test, Y_test)
    print(f'Träffsäkerhet: {accuracy * 100:.2f}%')
    ```

### Använd modellen på nytt data

1. Ladda ned filen `iris_new.data` och lägg in den i ditt projekt
2.  Läs in filen `iris_new.data` i ditt program

    ```python
    new_data = pd.read_csv('iris_new.data', header=None, names=['sepal_length', 'sepal_width', 'petal_length', 'petal_width'])
    ```
3.  Använd din modell för att kategorisera blommorna i `iris_new.data`

    ```python
    predictions = knn.predict(new_data)
    new_data['predicted_class'] = le.inverse_transform(predictions)
    print(new_data)
    ```

## Fullständig kod för KNN-modellen

```python
import pandas as pd
from sklearn.model_selection import train_test_split
from sklearn.preprocessing import LabelEncoder
from sklearn.neighbors import KNeighborsClassifier

# Läs in data från fil
df = pd.read_csv('iris.data', header=None, names=['sepal_length', 'sepal_width', 'petal_length', 'petal_width', 'class'])
print(df.head())

# Gör om class kolumnens text-data till tal
le = LabelEncoder()
df['class'] = le.fit_transform(df['class'])
print(df.head())

# Dela upp data i Label och Features
X = df.drop('class', axis=1)
Y = df['class']

# Dela upp data i träningsdata och testdata
X_train, X_test, Y_train, Y_test = train_test_split(X, Y, test_size=0.1, random_state=42)

# Skapa en KNN-modell med K=9
knn = KNeighborsClassifier(n_neighbors=9)

# Träna modellen
knn.fit(X_train, Y_train)

# Testa modellen och skriv ut träffsäkerheten
accuracy = knn.score(X_test, Y_test)
print(f'Träffsäkerhet: {accuracy * 100:.2f}%')

# Läs in nytt data
new_data = pd.read_csv('iris_new.data', header=None, names=['sepal_length', 'sepal_width', 'petal_length', 'petal_width'])

# Använd modellen för att förutspå klasser
predictions = knn.predict(new_data)
new_data['predicted_class'] = le.inverse_transform(predictions)
print(new_data)
```

## Förväntade utskrifter:

1.  **För träffsäkerhet:**

    ```
    Träffsäkerhet: 100.00%
    ```
2.  **För kategoriserade blommor i `iris_new.data`:**

    ```
    sepal_length  sepal_width  petal_length  petal_width predicted_class
    5.1           3.5          1.4           0.2          Iris-setosa
    7.0           3.2          4.7           1.4          Iris-versicolor
    6.3           3.3          6.0           2.5          Iris-virginica
    ```

Nu har du tränat en KNN-modell för att klassificera olika iris-blommor baserat på mått på deras sepal- och petal-blad, och du har använt modellen för att förutspå klassen för nya blomdata.
