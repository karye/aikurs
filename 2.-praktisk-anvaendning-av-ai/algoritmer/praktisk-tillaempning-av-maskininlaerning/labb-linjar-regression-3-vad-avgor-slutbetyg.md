# Labb -  Linjär regression 3: Vad avgör slutbetyg

{% file src="../../../.gitbook/assets/student-portugisiska.csv" %}

{% file src="../../../.gitbook/assets/student-portugisiska - new.csv" %}

I den här uppgiften ska du skapa en modell som kan förutspå slutbetyget (kolumn G3) för amerikanska elever i high school. Du kommer använda data från filen `student-portugisiska-slutbetyg.csv`, som innehåller information om studenters betyg, studievanor, familjesituation med mera.

## Steg för att skapa modellen

1. **Ladda ned data**
   * Ladda ned filen `student-portugisiska-slutbetyg.csv`.
2. **Databeskrivning**
   * Kolumner:
     * `school`: Studentens skola (binär: 'GP' - Gabriel Pereira eller 'MS' - Mousinho da Silveira)
     * `sex`: Studentens kön (binär: 'F' - kvinna eller 'M' - man)
     * `age`: Studentens ålder (numeriskt: från 15 till 22)
     * `address`: Studentens hemadresstyp (binär: 'U' - urban eller 'R' - landsbygd)
     * `famsize`: Familjestorlek (binär: 'LE3' - mindre eller lika med 3 eller 'GT3' - större än 3)
     * `Pstatus`: Föräldrars samboerstatus (binär: 'T' - bo tillsammans eller 'A' - från varandra)
     * `Medu`: Mors utbildning (numerisk: 0 - ingen, 1 - grundskoleutbildning, 2 - 5:e till 9:e klass, 3 - gymnasial utbildning, 4 - högre utbildning)
     * `Fedu`: Faders utbildning (numeriskt: 0 - ingen, 1 - grundskoleutbildning, 2 - 5:e till 9:e klass, 3 - gymnasieutbildning, 4 - högre utbildning)
     * `Mjob`: Moderjobb (nominellt: 'lärare', 'hälso- och sjukvårdsrelaterade', 'civila tjänster', 'vid\_home', 'other')
     * `Fjob`: Fars jobb (nominellt: 'lärare', 'hälso- och sjukvårdsrelaterad', 'civila tjänster', 'at\_home', 'annan')
     * `reason`: Anledning att välja denna skola (nominellt: nära "hem", skolans "rykte", "kurs" preferens, "annat")
     * `guardian`: Studentens vårdnadshavare (nominellt: "mor", "far", "annan")
     * `traveltime`: Hem till skolan restid (numeriskt: 1 - <15 min., 2 - 15 till 30 min., 3 - 30 min. till 1 timme, 4 - >1 timme)
     * `studytime`: Veckovis studietid (numerisk: 1 - <2 timmar, 2 - 2 till 5 timmar, 3 - 5 till 10 timmar, 4 - >10 timmar)
     * `failures`: Antal F i tidigare kurser (numeriskt: 1 <= n <3, annars 4)
     * `schoolsup`: Extra utbildningsstöd (binärt: ja eller nej)
     * `famsup`: Utbildningsstöd för familjen (binärt: ja eller nej)
     * `paid`: Extra betalda klasser inom kursen ämne (matematik eller portugisiska) (binär: ja eller nej)
     * `activities`: Aktiviteter utanför skolan (binär: ja eller nej)
     * `nursery`: Gick i förskolan (binär: ja eller nej)
     * `higher`: Vill ta högre utbildning (binär: ja eller nej)
     * `internet`: Internetåtkomst hemma (binär: ja eller nej)
     * `romantic`: Med ett romantiskt förhållande (binärt: ja eller nej)
     * `famrel`: Kvaliteten på familjerelationer (numerisk: från 1 - mycket dålig till 5 - utmärkt)
     * `freetime`: Fritid efter skolan (numerisk: från 1 - mycket lågt till 5 - mycket högt)
     * `goout`: Går ut med vänner (numeriskt: från 1 - mycket lågt till 5 - mycket högt)
     * `Dalc`: Alkoholkonsumtion på arbetsdagar (numeriskt: från 1 - mycket lågt till 5 - mycket hög)
     * `Walc`: Alkoholkonsumtion under helgen (numerisk: från 1 - mycket låg till 5 - mycket hög)
     * `health`: Nuvarande hälsostatus (numerisk: från 1 - mycket dålig till 5 - mycket bra)
     * `absences`: Antal skolfrånvaron (numeriskt: från 0 till 93)
     * `G1`: Betyg i årskurs 1 (numeriskt: från 0 till 20)
     * `G2`: Betyg i årskurs 2 (numeriskt: från 0 till 20)
     * `G3`: Betyg i årskurs 3 - slutbetyg (numeriskt: från 0 till 20, utgångsmål)

## Genomförande

1.  **Läs in data och förbered för modellering**

    ```python
    import pandas as pd

    # Läs in data från csv-fil
    df = pd.read_csv('student-portugisiska-slutbetyg.csv')

    # Visa kolumnnamn och de 5 första raderna
    print(df.columns)
    print(df.head())
    ```
2. **Välj Label och Features**
   * **Label**: `G3`
   * **Features**: Välj minst 10 kolumner med minst en som innehåller text-värden.
3.  **Kod för att välja Features och Label**

    ```python
    # Välj Features och Label
    features = ['G1', 'G2', 'studytime', 'failures', 'absences', 'schoolsup', 'famsup', 'paid', 'activities', 'internet']
    X = df[features]
    Y = df['G3']
    ```
4.  **Gör om text-data till numerisk data**

    ```python
    X = pd.get_dummies(X, drop_first=True)
    ```
5.  **Dela upp data i tränings- och testdata**

    ```python
    from sklearn.model_selection import train_test_split

    X_train, X_test, Y_train, Y_test = train_test_split(X, Y, test_size=0.2, random_state=42)
    ```
6.  **Skapa och träna modeller**

    ```python
    from sklearn.linear_model import LinearRegression
    import joblib

    best_score = 0
    best_model = None

    for i in range(10):
        model = LinearRegression()
        model.fit(X_train, Y_train)
        score = model.score(X_test, Y_test)
        print(f'Modell {i+1} noggrannhet: {score}')
        
        if score > best_score:
            best_score = score
            best_model = model

    # Spara den bästa modellen
    joblib.dump(best_model, 'best_model_portugisiska.pkl')
    ```
7.  **Ladda upp och använd den bästa modellen**

    ```python
    best_model = joblib.load('best_model_portugisiska.pkl')

    # Läs in nytt data
    new_data = pd.read_csv('student-portugisiska-slutbetyg_NY.csv')

    # Gör om text-data till numerisk data
    new_data = pd.get_dummies(new_data[features], drop_first=True)

    # Använd modellen för att förutspå slutbetyg
    predictions = best_model.predict(new_data)
    print(predictions)
    ```
8.  **Skriv ut koefficienter och intercept**

    ```python
    print(f'Koefficienter: {best_model.coef_}')
    print(f'Intercept: {best_model.intercept_}')
    ```

## Rapportdel

1. **Ange kolumner som du valt som Features (minst 10 st varav minst 1 måste innehålla text-värden) och beskriv kort varför:**
   * `G1`: Tidigare betyg kan indikera prestation.
   * `G2`: Näst senaste betyget kan också indikera prestation.
   * `studytime`: Studietid kan påverka betygen positivt.
   * `failures`: Antal misslyckanden kan påverka slutbetyget negativt.
   * `absences`: Frånvaro kan indikera lägre betyg på grund av mindre undervisningstid.
   * `schoolsup`: Extra utbildningsstöd kan hjälpa till att förbättra betygen.
   * `famsup`: Familjestöd kan ha en positiv inverkan på studentens prestation.
   * `paid`: Extra betalda klasser kan indikera en högre satsning på studierna.
   * `activities`: Aktiviteter utanför skolan kan påverka fritiden och därmed bety

gen.

* `internet`: Tillgång till internet kan vara en viktig resurs för studier.

2.  **Träna 10 modeller och Spara den bästa. Skriv ut träffsäkerheten för varje modell:**

    ```python
    for i in range(10):
        model = LinearRegression()
        model.fit(X_train, Y_train)
        score = model.score(X_test, Y_test)
        print(f'Modell {i+1} noggrannhet: {score}')
    ```
3. **Skriv ut den bästa modellens koefficienter och intercept:**
   * **Koefficienter:** `[värden här]`
   * **Intercept:** `[värde här]`
4. **Ange Modellens funktion enligt formel-mallen Y = k1 \* x1 + k2 \* x2 + k3 \* x3 + ... + kN \* xN + m**
   * **Y =** `k1 * G1 + k2 * G2 + k3 * studytime + k4 * failures + k5 * absences + k6 * schoolsup + k7 * famsup + k8 * paid + k9 * activities + k10 * internet + intercept`
5. **Utifrån din bästa modell vilka 3 Features påverkar Label mest och varför tror du att det är så?**
   * **Svar:**
     * `G1`: Tidigare betyg har stor påverkan eftersom det visar på en kontinuerlig prestation.
     * `G2`: Näst senaste betyget är en direkt indikator på hur studenten presterat strax innan slutbetyget.
     * `studytime`: Studietid påverkar betygen eftersom mer tid investerad i studier kan leda till bättre resultat.
6. **Utifrån din bästa modell vilka 3 Features påverkar Label minst och varför tror du att det är så?**
   * **Svar:**
     * `schoolsup`: Extra utbildningsstöd kanske inte alltid används effektivt.
     * `famsup`: Familjestöd kanske inte har en direkt inverkan på den dagliga studieprestationen.
     * `internet`: Tillgång till internet kan variera i hur mycket det faktiskt används för studier.

## Fullständig kod för `student_betyg.py`

```python
import pandas as pd
from sklearn.model_selection import train_test_split
from sklearn.linear_model import LinearRegression
import joblib

# Läs in data från csv-fil
df = pd.read_csv('student-portugisiska-slutbetyg.csv')

# Välj Features och Label
features = ['G1', 'G2', 'studytime', 'failures', 'absences', 'schoolsup', 'famsup', 'paid', 'activities', 'internet']
X = df[features]
Y = df['G3']

# Gör om text-data till numerisk data
X = pd.get_dummies(X, drop_first=True)

# Dela upp data i tränings- och testdata
X_train, X_test, Y_train, Y_test = train_test_split(X, Y, test_size=0.2, random_state=42)

# Skapa och träna modeller
best_score = 0
best_model = None

for i in range(10):
    model = LinearRegression()
    model.fit(X_train, Y_train)
    score = model.score(X_test, Y_test)
    print(f'Modell {i+1} noggrannhet: {score}')
    
    if score > best_score:
        best_score = score
        best_model = model

# Spara den bästa modellen
joblib.dump(best_model, 'best_model_portugisiska.pkl')

# Ladda upp och använd den bästa modellen
best_model = joblib.load('best_model_portugisiska.pkl')

# Läs in nytt data
new_data = pd.read_csv('student-portugisiska-slutbetyg_NY.csv')

# Gör om text-data till numerisk data
new_data = pd.get_dummies(new_data[features], drop_first=True)

# Använd modellen för att förutspå slutbetyg
predictions = best_model.predict(new_data)
print(predictions)

# Skriv ut koefficienter och intercept
print(f'Koefficienter: {best_model.coef_}')
print(f'Intercept: {best_model.intercept_}')
```

Denna guide ger en strukturerad och tydlig metod för att skapa och testa en linjär regressionsmodell för att förutspå slutbetyg baserat på studentdata. Stegen innehåller kodexempel som är lätta att följa och implementera.
