# Labb - Linjär Regression 2: Vad avgör slutbetyg

I den här uppgiften ska du skapa en modell som kan förutspå slutbetyget (kolumn G3) för amerikanska elever i high school.

{% file src="../../../.gitbook/assets/student-mat.csv" %}

{% file src="../../../.gitbook/assets/student-mat - new.csv" %}

## Steg för att skapa modellen

1. **Ladda ned data**
   * Ladda ned filen `student-matte-slutbetyg.csv`.
2. **Databeskrivning**
   * Kolumner:
     * `school`: Studentens skola (binär: 'GP' - Gabriel Pereira eller 'MS' - Mousinho da Silveira)
     * `sex`: Studentens kön (binär: 'F' - kvinna eller 'M' - man)
     * `age`: Studentens ålder (numeriskt: från 15 till 22)
     * `address`: Studentens hemadresstyp (binär: 'U' - urban eller 'R' - landsbygd)
     * `famsize`: Familjestorlek (binär: 'LE3' - mindre eller lika med 3 eller 'GT3' - större än 3)
     * `Pstatus`: Föräldrars samboerstatus (binär: 'T' - bo tillsammans eller 'A' - från varandra)
     * `Medu`: Mors utbildning (numerisk: 0 - ingen, 1 - grundskoleutbildning, 2 - 5:e till 9:e klass, 3 - gymnasial utbildning, 4 - högre utbildning)
     * `Fedu`: Faders utbildning (numeriskt: 0 - ingen, 1 - grundskoleutbildning, 2 - 5:e till 9:e klass, 3 - gymnasieutbildning, 4 - högre utbildning)
     * `Mjob`: Moderjobb (nominellt: 'lärare', 'hälso- och sjukvårdsrelaterade', 'civila tjänster', 'vid\_home', 'other')
     * `Fjob`: Fars jobb (nominellt: 'lärare', 'hälso- och sjukvårdsrelaterad', 'civila tjänster', 'at\_home', 'annan')
     * `reason`: Anledning att välja denna skola (nominellt: nära "hem", skolans "rykte", "kurs" preferens, "annat")
     * `guardian`: Studentens vårdnadshavare (nominellt: "mor", "far", "annan")
     * `traveltime`: Hem till skolan restid (numeriskt: 1 - <15 min., 2 - 15 till 30 min., 3 - 30 min. till 1 timme, 4 - >1 timme)
     * `studytime`: Veckovis studietid (numerisk: 1 - <2 timmar, 2 - 2 till 5 timmar, 3 - 5 till 10 timmar, 4 - >10 timmar)
     * `failures`: Antal F i tidigare kurser (numeriskt: 1 <= n <3, annars 4)
     * `schoolsup`: Extra utbildningsstöd (binärt: ja eller nej)
     * `famsup`: Utbildningsstöd för familjen (binärt: ja eller nej)
     * `paid`: Extra betalda klasser inom kursen ämne (matematik eller portugisiska) (binär: ja eller nej)
     * `activities`: Aktiviteter utanför skolan (binär: ja eller nej)
     * `nursery`: Gick i förskolan (binär: ja eller nej)
     * `higher`: Vill ta högre utbildning (binär: ja eller nej)
     * `internet`: Internetåtkomst hemma (binär: ja eller nej)
     * `romantic`: Med ett romantiskt förhållande (binärt: ja eller nej)
     * `famrel`: Kvaliteten på familjerelationer (numerisk: från 1 - mycket dålig till 5 - utmärkt)
     * `freetime`: Fritid efter skolan (numerisk: från 1 - mycket lågt till 5 - mycket högt)
     * `goout`: Går ut med vänner (numeriskt: från 1 - mycket lågt till 5 - mycket högt)
     * `Dalc`: Alkoholkonsumtion på arbetsdagar (numeriskt: från 1 - mycket lågt till 5 - mycket hög)
     * `Walc`: Alkoholkonsumtion under helgen (numerisk: från 1 - mycket låg till 5 - mycket hög)
     * `health`: Nuvarande hälsostatus (numerisk: från 1 - mycket dålig till 5 - mycket bra)
     * `absences`: Antal skolfrånvaron (numeriskt: från 0 till 93)
     * `G1`: Betyg i årskurs 1 (numeriskt: från 0 till 20)
     * `G2`: Betyg i årskurs 2 (numeriskt: från 0 till 20)
     * `G3`: Betyg i årskurs 3 - slutbetyg (numeriskt: från 0 till 20, utgångsmål)

## Genomförande

1.  **Läs in data och förbered för modellering**

    ```python
    import pandas as pd

    # Läs in data från csv-fil
    df = pd.read_csv('student-matte-slutbetyg.csv')

    # Visa kolumnnamn och de 5 första raderna
    print(df.columns)
    print(df.head())
    ```
2. **Välj Label och Features**
   * **Label**: `G3`
   * **Features**: Välj 5 kolumner med numeriska värden
3.  **Kod för att välja Features och Label**

    ```python
    # Välj Features och Label
    features = ['G1', 'G2', 'studytime', 'failures', 'absences']
    X = df[features]
    Y = df['G3']
    ```
4.  **Dela upp data i tränings- och testdata**

    ```python
    from sklearn.model_selection import train_test_split

    X_train, X_test, Y_train, Y_test = train_test_split(X, Y, test_size=0.2, random_state=42)
    ```
5.  **Välj modell/algoritm och läs in den**

    ```python
    from sklearn.linear_model import LinearRegression

    # Skapa modellen
    model = LinearRegression()
    ```
6.  **Träna modellen**

    ```python
    # Träna modellen
    model.fit(X_train, Y_train)
    ```
7.  **Testa modellen och spara resultatet**

    ```python
    # Testa modellen
    score = model.score(X_test, Y_test)
    print(f'Modellens noggrannhet: {score}')
    ```
8.  **Använd modellen för att förutsäga nya värden**

    * Använd data från `student-matte-slutbetyg_NY.csv` för att förutsäga slutbetyg baserat på studentdata.

    ```python
    # Läs in nytt data
    new_data = pd.read_csv('student-matte-slutbetyg_NY.csv')

    # Använd modellen för att förutsäga slutbetyg
    predictions = model.predict(new_data[features])
    print(predictions)
    ```
9.  **Skriv ut funktionens intercept och koefficienter**

    ```python
    print(f'Intercept: {model.intercept_}')
    print(f'Koefficienter: {model.coef_}')
    ```

## RAPPORT-DEL

1. **Ange kolumn som du valt som Label:**
   * `G3`
2. **Ange kolumner som du valt som Features (5 st med talvärden) och beskriv kort varför:**
   * `G1`: Tidigare betyg kan indikera prestation.
   * `G2`: Näst senaste betyget kan också indikera prestation.
   * `studytime`: Studietid kan påverka betygen positivt.
   * `failures`: Antal misslyckanden kan påverka slutbetyget negativt.
   * `absences`: Frånvaro kan indikera lägre betyg på grund av mindre undervisningstid.
3. **Ange vilken modell du använder:**
   * `Linear Regression`
4. **Ange hur stor del i procent som du använder för träningsdata och hur stor andel testdata:**
   * Träningsdata: 80%
   * Testdata: 20%
5.  **Använd data modellen för att förutsäga betyg för datat i `student-matte-slutbetyg_NY.csv`**

    * **Förutsägelser:**

    ```python
    for index, prediction in enumerate(predictions):
        print(f'Student {index + 1} -> Förutsagt slutbetyg: {prediction:.2f}')
    ```

## Fullständig kod för `student_betyg.py`

```python
import pandas as pd
from sklearn.model_selection import train_test_split
from sklearn.linear_model import LinearRegression

# Läs in data från csv-fil
df = pd.read_csv('student-matte-sl

utbetyg.csv')

# Välj Features och Label
features = ['G1', 'G2', 'studytime', 'failures', 'absences']
X = df[features]
Y = df['G3']

# Dela upp data i tränings- och testdata
X_train, X_test, Y_train, Y_test = train_test_split(X, Y, test_size=0.2, random_state=42)

# Skapa och träna modellen
model = LinearRegression()
model.fit(X_train, Y_train)

# Testa modellen och skriv ut träff-resultatet
score = model.score(X_test, Y_test)
print(f'Modellens noggrannhet: {score}')

# Läs in nytt data
new_data = pd.read_csv('student-matte-slutbetyg_NY.csv')

# Använd modellen för att förutspå slutbetyg
predictions = model.predict(new_data[features])

# Skriv ut förutsägelserna
for index, prediction in enumerate(predictions):
    print(f'Student {index + 1} -> Förutsagt slutbetyg: {prediction:.2f}')

# Skriv ut funktionens intercept och koefficienter
print(f'Intercept: {model.intercept_}')
print(f'Koefficienter: {model.coef_}')
```

Denna guide ger en strukturerad och tydlig metod för att skapa och testa en linjär regressionsmodell för att förutspå slutbetyg baserat på studentdata. Stegen innehåller kodexempel som är lätta att följa och implementera.
