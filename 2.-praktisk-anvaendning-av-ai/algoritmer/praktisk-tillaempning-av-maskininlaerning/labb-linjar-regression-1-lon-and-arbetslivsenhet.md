# Labb - Linjär Regression 1: Lön & arbetslivsenhet

I den här uppgiften ska du skapa en modell som kan förutspå vilken lön en person har beroende på arbetslivserfarenhet (hur många år man har jobbat). Du kommer att träna en linjär regressionsmodell för att förutspå löner baserat på arbetslivserfarenhet.

{% file src="../../../.gitbook/assets/Salary_Data.csv" %}

{% file src="../../../.gitbook/assets/Salary_Data _NEW.csv" %}

## Steg för att skapa modellen 

1. **Ladda ned data**
   * Ladda ned filen `salary_data.csv`.
   * Databeskrivning:
     * `YearsExperience`: Decimaltal av antal år av arbetslivserfarenhet.
     * `Salary`: Decimaltal årslön i dollar.
2. **Förbered projektet**
   * Öppna PyCharm och använd ditt maskininlärningsprojekt.
   * Skapa en ny Python-fil, exempelvis `linear_e.py`.
   * Lägg till filen `salary_data.csv` i ditt Python-projekt.
3.  **Läs in och förbered data**

    ```python
    import pandas as pd

    # Läs in data från csv-fil
    df = pd.read_csv('salary_data.csv')

    # Visa kolumnnamn och de 5 första raderna
    print(df.columns)
    print(df.head())
    ```
4.  **Sätt Label (Y) och Feature (X)**

    ```python
    # Sätt Feature och Label
    X = df[['YearsExperience']]
    Y = df['Salary']
    ```
5.  **Dela upp data i tränings- och testdata**

    ```python
    from sklearn.model_selection import train_test_split

    X_train, X_test, Y_train, Y_test = train_test_split(X, Y, test_size=0.2, random_state=42)
    ```
6.  **Skapa och träna modellen**

    ```python
    from sklearn.linear_model import LinearRegression

    # Skapa modellen
    model = LinearRegression()

    # Träna modellen
    model.fit(X_train, Y_train)
    ```
7.  **Testa modellen och skriv ut träff-resultatet**

    ```python
    # Testa modellen
    score = model.score(X_test, Y_test)
    print(f'Modellens noggrannhet: {score}')
    ```
8.  **Använd modellen för att förutsäga löner för nytt data**

    * Ladda ned det nya datat `salary_data_new.csv` som bara innehåller arbetslivserfarenhet.
    * Läs in det nya datat i ditt program och använd den tränade modellen för att förutspå löner.

    ```python
    # Läs in nytt data
    new_data = pd.read_csv('salary_data_new.csv')

    # Använd modellen för att förutspå löner
    predictions = model.predict(new_data[['YearsExperience']])
    print(predictions)
    ```
9.  **Skriv ut funktionens intercept och koefficienter**

    ```python
    print(f'Intercept: {model.intercept_}')
    print(f'Koefficienter: {model.coef_}')
    ```

## RAPPORT-DEL

1. **Ange kolumn för Label:**
   * `Salary`
2. **Ange kolumn för Feature:**
   * `YearsExperience`
3.  **Vilka förutsägelser fick du från din modell när den läste in värden?**

    ```python
    for exp, sal in zip(new_data['YearsExperience'], predictions):
        print(f'Arbetslivserfarenhet: {exp} år -> Förutsagd lön: ${sal:.2f}')
    ```
4. **Ser du något mönster, hänger arbetslivserfarenhet och lön ihop?**
   * Ja, mönstret visar att lönen ökar med arbetslivserfarenheten. Personer med fler år av arbetslivserfarenhet tenderar att ha högre löner än de med färre år av erfarenhet.

## Fullständig kod för `linear_e.py`

```python
import pandas as pd
from sklearn.model_selection import train_test_split
from sklearn.linear_model import LinearRegression

# Läs in data från csv-fil
df = pd.read_csv('salary_data.csv')

# Sätt Feature och Label
X = df[['YearsExperience']]
Y = df['Salary']

# Dela upp data i tränings- och testdata
X_train, X_test, Y_train, Y_test = train_test_split(X, Y, test_size=0.2, random_state=42)

# Skapa och träna modellen
model = LinearRegression()
model.fit(X_train, Y_train)

# Testa modellen och skriv ut träff-resultatet
score = model.score(X_test, Y_test)
print(f'Modellens noggrannhet: {score}')

# Läs in nytt data
new_data = pd.read_csv('salary_data_new.csv')

# Använd modellen för att förutspå löner
predictions = model.predict(new_data[['YearsExperience']])

# Skriv ut förutsägelserna
for exp, sal in zip(new_data['YearsExperience'], predictions):
    print(f'Arbetslivserfarenhet: {exp} år -> Förutsagd lön: ${sal:.2f}')

# Skriv ut funktionens intercept och koefficienter
print(f'Intercept: {model.intercept_}')
print(f'Koefficienter: {model.coef_}')
```

Detta upplägg ger en tydlig och strukturerad metod för att skapa och testa en linjär regressionsmodell för att förutspå löner baserat på arbetslivserfarenhet. Stegen är enkla att följa och implementera, och ger en bra grund för att förstå hur linjär regression kan användas för att analysera data.
