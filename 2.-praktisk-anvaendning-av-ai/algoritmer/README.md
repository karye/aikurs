# Maskininlärning

{% file src="../../.gitbook/assets/lek 1 AI introp historia.pptx.pdf" %}

## Aktiviteter

7. **Teachable Machine**
    * Prova [Teachable Machine](https://teachablemachine.withgoogle.com/), där du kan träna en AI-modell att känna igen olika objekt. Detta är ett bra sätt att förstå hur AI kan tränas och lära sig.
    * Instruktion: Träna AI-modellen att känna igen olika objekt och testa dess förmåga.