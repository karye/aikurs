# Uppgift - Installation av verktyg

Du behöver ha Python och PyCharm installerat på din dator innan du börjar denna uppgift. I den här uppgiften ska du ha följande verktyg:

## **Anaconda** (installeras som en del av övningen)

### Steg för att skapa en miljö för maskininlärningsprojekt

1. **Vad är en miljö?**
   * En miljö i Python används för att koda ett program.
   * En miljö bestämmer vilken Python-version du använder och vilka Python-paket du använder.
   * Paket är nedladdningsbara bibliotek för olika ändamål, till exempel matematik och maskininlärning.
   * Du har hitintills använt en standardmiljö som enbart består av Python 3.6 eller senare versioner.
   * För att ett projekt ska ha tillgång till nödvändig funktionalitet utöver standardbiblioteket, skapar du en miljö och installerar de paket du behöver.
2. **Installera Anaconda**
   * Anaconda är en samling verktyg som används för maskininlärning och att skapa miljöer.
   * Ladda ned Anaconda från [Anaconda Download](https://www.anaconda.com/download/)
   * Installera Anaconda och se till att lägga till Anaconda i Windows sökväg under installationen.
3.  **Skapa miljön för att utveckla maskininlärningsprogram**

    * Skapa en miljö bestående av Python version 3.7 och ett antal paket för maskininlärningsprogram.

    ```bash
    # Kontrollera vilken Python-version du har:
    python --version
    ```

    * Versionen ska vara 3.6 eller 3.7. Skriv ner din Python-version.
    * Skapa en virtuell miljö för att köra Python 3.6 eller 3.7 med rätt verktyg:

    ```bash
    conda create -n machinelearning python=3.7
    ```
4.  **Aktivera miljön**

    ```bash
    conda activate machinelearning
    ```
5.  **Installera paket till miljön**

    * Installera viktiga bibliotek för maskininlärning:

    ```bash
    pip install tensorflow
    pip install scikit-learn
    pip install numpy
    pip install keras
    pip install pandas
    pip install pickle
    ```
6.  **Ta reda på vart din miljö har blivit installerad**

    ```bash
    echo %CONDA_PREFIX%
    ```

    * Skriv ner sökvägen till din miljö, t.ex.: `C:\Users\ditt_användarnamn\anaconda3\envs\machinelearning\`

## Skapa projektet och välj interpreterare i PyCharm

1. **Öppna PyCharm**
2. **Skapa ett nytt tomt projekt**
   * Döp mappen till något vettigt, exempelvis `machinelearning`.
   * Välj en existerande interpreterare för tillfället.
   * Klicka på "Create".
3. **Välj miljö**
   * Gå till `File -> Settings`
   * Välj fliken på sidan ”Python Interpreter”.
   * Klicka på kugghjulet på övre högersidan och välj "Add".
   * Välj "Conda-fliken" på vänster sida.
   * Välj "Existing interpreter" och välj din existerande miljö `machinelearning`.
   * Klicka på `...` för att välja Python interpreteraren i din miljö.
   * Gå till mappen där din miljö ligger: `C:\Users\ditt_användarnamn\anaconda3\envs\machinelearning\`
   * Välj `pythonw.exe` som interpreterare.
   * Klicka "OK".
4. **Skapa en Python-fil för ditt projekt**
   * Döp filen till exempelvis `maskinuppg1.py`.
5. **Konfigurera projektet**
   * Klicka på "Add Configuration".
   * Klicka på `+` tecknet för att skapa en ny konfiguration.
   * Välj "Python".
   * Döp konfigurationen till något som `machine_learning_konf`.
   * Ange din nyskapade Python-fil för projektet.
   * Hitta din nyskapade Python-fil och tryck "OK" och "OK" igen på konfigurationsdialogen.
6.  **Skriv och kompilera koden**

    * Öppna din Python-fil och skriv:

    ```python
    import tensorflow as tf
    print(tf.__version__)
    ```

    * Kompilera filen. Ignorera eventuella varningar om CUDA och GPU, då de inte påverkar de flesta grundläggande projekt.

## Nu har du satt upp miljön för att utveckla maskininlärningsprogram och är klar!
