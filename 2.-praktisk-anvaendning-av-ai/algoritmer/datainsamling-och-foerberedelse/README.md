# Datainsamling och databehandling

## Mål
Denna lektion syftar till att ge eleverna praktisk erfarenhet av datainsamling och förberedelse, två avgörande steg i alla AI-projekt. Genom att engagera eleverna i en hands-on uppgift, förstår de bättre vikten av kvalitativ data och lär sig grunderna i hur man hanterar och förbereder data för analys.

{% file src="../../.gitbook/assets/DataBehandling_v2 enklare.pptx.pdf" %}

## Innehåll

* **Betydelsen av data i AI**: Förklara hur data är grunden för alla maskininlärningsmodeller och hur kvaliteten på data påverkar resultatet.
* **Datainsamling**: Diskutera metoder för att samla in data, inklusive observationer, enkäter och användning av offentliga dataset.
* **Dataförberedelse**: Introducera grundläggande steg för att förbereda data för maskininlärning, såsom rensning av data, hantering av saknade värden och normalisering.
* **Etik och sekretess i datahantering**: Ta upp viktiga etiska överväganden och sekretessfrågor vid insamling och användning av data.
* **Vad är databehandling**: Förklara betydelsen av dataanalys i AI-processen, med fokus på att omvandla rådata till användbar information.
* **Verktyg för databehandling**: Presentera Excel och Google Sheets som lättillgängliga verktyg för grundläggande dataanalys. För mer avancerade elever, introducera Python med Pandas-biblioteket.
* **Utforska grundläggande statistik**: Gå igenom statistiska begrepp som medelvärde, median och standardavvikelse.
* **Visualisering av data**: Visa hur man skapar enkla diagram som stapeldiagram och linjediagram för att visualisera datatrender.

## Aktivitet

* **Datainsamlingsprojekt**: Eleverna delas in i små grupper och får i uppdrag att samla in en specifik typ av data. Det kan vara allt från en enkel enkät till observationer av något fenomen. Uppmuntra dem att använda verktyg som Google Formulär eller Excel för att samla och organisera sin data.
* **Förbereda för databehandling**: Eleverna rensar och förbereder sin insamlade data för eventuell analys i framtida lektioner.
* **Diskussionsfrågor**:
  * Vilka utmaningar stötte ni på under datainsamlingen?
  * Hur tror ni kvaliteten på er data kommer att påverka era slutresultat i maskininlärningsprojektet?
* **Lärarens feedback**: Ge feedback på elevernas datainsamlingsmetoder och hur de hanterade eventuella utmaningar.

