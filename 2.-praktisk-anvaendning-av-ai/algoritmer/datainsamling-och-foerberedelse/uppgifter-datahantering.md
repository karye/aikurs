# Uppgifter - Datahantering

## Stegen för databearbetning

1. **Ladda ned data**
2. **Hitta Label och Features i datat**
3. **Läs in data i Python**
4. **Ta bort onödigt data**
5. **Dela upp data i Label och Features**
6. **Dela upp data i tränings- och testdata**

### Filer

{% file src="../../../.gitbook/assets/USA_Housing.csv" %}

{% file src="../../../.gitbook/assets/London.csv" %}

### UPPGIFT A: Förutsäg huspriser i USA

En mäklare vill ha hjälp med att förutsäga huspriset för städer i USA. Priset påverkas av:

* Hur många bor i staden
* Medelinkomsten i staden
* Hur gamla husen är
* Hur många rum husen har i genomsnitt
* Hur många sovrum husen har i genomsnitt

## Förberedelser

1. **Starta PyCharm**
2. **Skapa ett nytt ML-projekt** med din miljö `machinelearning`.

## Stegen

1. **Hitta data**
   * Datat finns i filen `USA_Housing.csv`.
   * Ladda ned csv-filen och placera den i din projektmapp.
2. **Kolumnbeskrivning**
   * `Avg. Area Income`: Genomsnittlig inkomst för hushållet i staden.
   * `Avg. Area House Age`: Genomsnittlig ålder på husen i staden.
   * `Avg. Area Number of Rooms`: Genomsnittligt antal rum för husen i staden.
   * `Avg. Area Number of Bedrooms`: Genomsnittligt antal sovrum för husen i staden.
   * `Area Population`: Stadens befolkning.
   * `Price`: Husets försäljningspris.
   * `Address`: Husets adress.
3. **Hitta Label och Features**
   * **Label**: `Price`
   * **Features**: `Avg. Area Income`, `Avg. Area House Age`, `Avg. Area Number of Rooms`, `Avg. Area Number of Bedrooms`, `Area Population`
4. **Läs in datat i Python**
   * Skapa ett Python-projekt som använder miljön du skapade tidigare.
   * Kopiera filen `USA_Housing.csv` till din projektmapp.
5.  **Kod för att läsa in data**

    ```python
    import pandas as pd

    # Läs in data från csv-fil
    df = pd.read_csv('USA_Housing.csv', sep=',')

    # Visa kolumnnamn och de 5 första raderna
    print(df.columns)
    print(df.head())
    ```
6.  **Ta bort onödigt data**

    ```python
    # Ta bort kolumnen 'Address'
    df = df.drop(['Address'], axis=1)
    ```
7.  **Dela upp data i Label och Features**

    ```python
    # Spara Label i Y och Features i X
    X = df[['Avg. Area Income', 'Avg. Area House Age', 'Avg. Area Number of Rooms', 'Avg. Area Number of Bedrooms', 'Area Population']]
    Y = df['Price']
    ```
8.  **Dela upp data i tränings- och testdata**

    ```python
    from sklearn.model_selection import train_test_split

    X_train, X_test, Y_train, Y_test = train_test_split(X, Y, test_size=0.2, random_state=42)
    ```

### UPPGIFT B: Förutsäg huspriser i London

Nu har mäklaren fixat data på tidigare hus och lägenhetsförsäljningar i London. Han vill ha hjälp att kunna förutse priset med hjälp av data på själva huset/lägenheten från London-området.

1. **Förberedelser**
   * Använd samma projekt men skapa en ny fil, t.ex. `uppgift2.py`.
   * Ladda ned csv-filen `London.csv` och placera den i din projektmapp.
2. **Kolumnbeskrivning**
   * `Property Name`: Gata
   * `Price`: Pris
   * `House Type`: Typ av boende (lägenhet, hus, parhus)
   * `Area in sq ft`: Yta i kvadratfot
   * `No. of Bedrooms`: Antal sovrum
   * `No. of Bathrooms`: Antal badrum
   * `No. of Receptions`: Antal ingångar
   * `Location`: Stadsdel
   * `City/County`: Stad
   * `Postal Code`: Postnummer
3. **Hitta Label och Features**
   * **Label**: `Price`
   * **Features**: `Area in sq ft`, `No. of Bedrooms`, `No. of Bathrooms`, `No. of Receptions`, `Location`, `City/County`, `Postal Code`, `House Type`
4.  **Läs in data i Python**

    ```python
    import pandas as pd

    # Läs in data från csv-fil
    df_london = pd.read_csv('London.csv', sep=',')

    # Visa kolumnnamn och de 5 första raderna
    print(df_london.columns)
    print(df_london.head())
    ```
5.  **Ta bort onödigt data**

    ```python
    # Ta bort kolumnen 'Property Name'
    df_london = df_london.drop(['Property Name'], axis=1)
    ```
6.  **Gör om textdata till siffror**

    ```python
    # Konvertera 'House Type' till siffror
    df_london['House Type'] = df_london['House Type'].astype('category').cat.codes
    ```
7.  **Dela upp data i Label och Features**

    ```python
    # Spara Label i Y och Features i X
    X_london = df_london[['Area in sq ft', 'No. of Bedrooms', 'No. of Bathrooms', 'No. of Receptions', 'Location', 'City/County', 'Postal Code', 'House Type']]
    Y_london = df_london['Price']
    ```
8.  **Dela upp data i tränings- och testdata**

    ```python
    from sklearn.model_selection import train_test_split

    X_train_london, X_test_london, Y_train_london, Y_test_london = train_test_split(X_london, Y_london, test_size=0.2, random_state=42)
    ```

## Sammanfattning

* Du har nu två uppgifter som hjälper dig att förutsäga huspriser, en för USA och en för London.
* Följ stegen noggrant för att bearbeta data, skapa modeller och dela upp datat i tränings- och testdata.
* Se till att ha rätt verktyg installerade och att använda den specifika miljön du skapat för maskininlärning.
