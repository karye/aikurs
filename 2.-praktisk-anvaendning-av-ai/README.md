# 2. Praktisk användning av AI

* Översikt över processen för problemlösning med AI.
* Enklare typ av problemlösning med hjälp av AI, till exempel klassificering, objektigenkänning, prediktion, tolkning och bearbetning av naturligt språk (NLP), enklare maskininlärning och användning av spelagent.
* Principer för, och tillämpningar av, vanliga tekniker inom AI, däribland maskininlärning och robotik.
* Metoder för enklare träning av AI.
