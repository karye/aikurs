# Neurala nätverk

Ett exempel på en praktisk tillämpning av neurala nätverk är att finjustera en språkmodell för att skapa en AI-assistent som kan generera text baserat på användarens input. I detta exempel använder vi LM Studio, en webbaserad plattform för att finjustera språkmodeller med minimal kodning.

{% embed url="https://youtu.be/zjkBMFhNj_g?si=O-OBRcJ9qupSuwB4" %}

