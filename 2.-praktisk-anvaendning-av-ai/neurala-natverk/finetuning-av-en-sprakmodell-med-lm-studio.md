# Finetuning av en språkmodell med LM Studio

## Syfte

Eleverna ska lära sig hur man finjusterar (finetuning) en befintlig språkmodell för att förbättra dess prestanda på specifika uppgifter eller anpassa den för ett specifikt domänområde.

## Mål

* Förstå vad finetuning av en språkmodell innebär.
* Lära sig använda LM Studio för att finjustera en befintlig modell.
* Genomföra praktiska övningar där eleverna finjusterar en modell med en egen dataset.
* Analysera och diskutera resultaten från finetuning-processen.

## Bakgrund

Finetuning är processen där en befintlig språkmodell tränas vidare på en specifik dataset för att förbättra dess prestanda inom ett visst område eller för en specifik uppgift. Detta är användbart när den befintliga modellen inte presterar optimalt för en viss användning eller när man vill specialisera modellen för en viss typ av text.

## Instruktioner

### Förberedelser före lektionen

1. **Förbereda LM Studio installationspaket**: Säkerställ att alla elever har tillgång till installationspaketet för LM Studio. Detta kan laddas ner från [LM Studio's officiella hemsida](https://www.lmstudio.com/download).
2. **Val av språkmodell**: Vi kommer att använda GPT-Sw3 356M Instruct som språkmodell för denna övning.
3. **Dataset för finetuning**: Förbered en dataset som eleverna ska använda för att finjustera modellen. Det kan vara en samling texter inom ett specifikt ämnesområde (t.ex. tekniska artiklar, medicinska journaler, eller litterära verk).

## Steg-för-steg Planering

### Steg 1: Introduktion till finetuning (10 minuter)

* Förklara vad finetuning av en språkmodell innebär.
* Diskutera varför och när det är användbart att finjustera en modell.
* Introducera den dataset som kommer att användas för finetuning.

### Steg 2: Installation av LM Studio (15 minuter)

* Be eleverna att öppna sina bärbara datorer.
* Ge instruktioner om hur man laddar ner och installerar LM Studio:
  1. Gå till [LM Studio's officiella hemsida](https://www.lmstudio.com/download).
  2. Välj rätt version för ditt operativsystem (Windows, macOS, Linux).
  3. Följ installationsanvisningarna för att installera LM Studio på din dator.

### Steg 3: Installera och förbereda språkmodellen (15 minuter)

* Instruera eleverna att starta LM Studio efter installationen.
* Visa hur man laddar ner och installerar GPT-Sw3 356M Instruct-modellen:
  1. Öppna LM Studio.
  2. Navigera till sektionen för att lägga till nya modeller.
  3. Välj GPT-Sw3 356M Instruct från listan och klicka på "Install".
  4. Vänta tills modellen har laddats ner och installerats.

### Steg 4: Förbereda dataset för finetuning (10 minuter)

* Visa eleverna hur man laddar upp och förbereder datasetet för finetuning i LM Studio.
  1. Samla texterna i en fil eller flera filer och se till att de är i rätt format (t.ex. .txt eller .csv).
  2. Ladda upp datasetet till LM Studio.

### Steg 5: Finetuning-processen (30 minuter)

* Ge steg-för-steg instruktioner för hur man finjusterar modellen med hjälp av datasetet:
  1. Öppna LM Studio och navigera till sektionen för modellträning.
  2. Välj GPT-Sw3 356M Instruct som grundmodell.
  3. Ladda upp det förberedda datasetet.
  4. Konfigurera träningsparametrar som batch size, lärandegrad och antal epoker.
  5. Starta träningsprocessen och övervaka dess framsteg.

### Steg 6: Testa och analysera finetunade modellen (20 minuter)

* Efter att finetuning-processen är klar, visa hur man testar modellen med ny data.
  1. Skapa en ny textfil med några exempel på frågor eller uppgifter.
  2. Använd den finetunade modellen för att generera svar eller lösa uppgifterna.
* Diskutera resultaten med eleverna:
  * Hur har modellens prestanda förbättrats?
  * Vad är styrkorna och svagheterna med den finetunade modellen?

## Övningar

### Övning 1: Finetuning för specifika domänområden

* Dela in eleverna i grupper och ge varje grupp en annan dataset inom olika domänområden (t.ex. juridiska texter, medicinska journaler, tekniska manualer).
* Be varje grupp finjustera modellen med sin specifika dataset och sedan testa modellen.
* Diskutera skillnaderna i modellens prestanda beroende på vilket domänområde den har finjusterats för.

### Övning 2: Anpassa modellen för specifika uppgifter

* Be eleverna att samla egna data inom ett område de är intresserade av (t.ex. poesi, nyhetsartiklar, produktrecensioner).
* Låt dem finjustera modellen med sina egna data och sedan testa hur modellen presterar på uppgifter relaterade till deras data.

### Utvärdering

* Bedöm elevernas förståelse baserat på deras förmåga att finjustera och använda modellen.
* Analysera hur de reflekterar över finetuning-processen och deras diskussioner om resultaten.

### Material som behövs

* Bärbara datorer med internetuppkoppling
* LM Studio installationspaket
* Förberett dataset för finetuning
* Instruktioner för installation och användning (kan delas ut som en PDF)

### Lektionsplanering sammanfattning

1. **Introduktion till finetuning**
2. **Installation av LM Studio**
3. **Installera och förbereda språkmodellen**
4. **Förbereda dataset för finetuning**
5. **Genomföra finetuning-processen**
6. **Testa och analysera finetunade modellen**

Denna planering ger eleverna en djupare förståelse för hur man kan anpassa och förbättra språkmodeller för specifika behov och användningsområden, samt praktisk erfarenhet av att arbeta med avancerade maskininlärningstekniker.
